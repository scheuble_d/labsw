﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="18008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Classes" Type="Folder">
			<Item Name="Device" Type="Folder">
				<Item Name="Device.lvclass" Type="LVClass" URL="../Device.lvclass"/>
				<Item Name="MFC.lvclass" Type="LVClass" URL="../MFC.lvclass"/>
				<Item Name="MFC MIX.lvclass" Type="LVClass" URL="../MFC MIX.lvclass"/>
				<Item Name="VirtualDevice.lvclass" Type="LVClass" URL="../VirtualDevice.lvclass"/>
			</Item>
			<Item Name="Setpoint Measurement" Type="Folder">
				<Item Name="Setpoint.lvclass" Type="LVClass" URL="../Setpoint.lvclass"/>
				<Item Name="Measurement.lvclass" Type="LVClass" URL="../Measurement.lvclass"/>
				<Item Name="readMoS.vi" Type="VI" URL="../readMoS.vi"/>
				<Item Name="getNumOfMoS.vi" Type="VI" URL="../getNumOfMoS.vi"/>
				<Item Name="readMaS.vi" Type="VI" URL="../readMaS.vi"/>
			</Item>
			<Item Name="Connection" Type="Folder">
				<Item Name="Connection.lvclass" Type="LVClass" URL="../Connection.lvclass"/>
				<Item Name="ConnectionTypes.ctl" Type="VI" URL="../Helper/ConnectionTypes.ctl"/>
				<Item Name="ConnectionInfoList.ctl" Type="VI" URL="../ConnectionInfoList.ctl"/>
			</Item>
			<Item Name="Datapoint" Type="Folder">
				<Item Name="Datapoint.lvclass" Type="LVClass" URL="../Datapoint.lvclass"/>
			</Item>
			<Item Name="createDevice.vi" Type="VI" URL="../Helper/createDevice.vi"/>
		</Item>
		<Item Name="DeviceConfigurations" Type="Folder">
			<Item Name="MFC1.ini" Type="Document" URL="../MFC1.ini"/>
		</Item>
		<Item Name="Global Functions" Type="Folder">
			<Item Name="openTDMSfromSettings.vi" Type="VI" URL="../openTDMSfromSettings.vi"/>
			<Item Name="Settings.lvclass" Type="LVClass" URL="../Settings.lvclass"/>
			<Item Name="Setting.lvclass" Type="LVClass" URL="../Setting.lvclass"/>
		</Item>
		<Item Name="Manager" Type="Folder">
			<Item Name="Add DEVICE to array.vi" Type="VI" URL="../Add DEVICE to array.vi"/>
			<Item Name="getDevice.vi" Type="VI" URL="../getDevice.vi"/>
		</Item>
		<Item Name="GlobalSettings.ini" Type="Document" URL="../GlobalSettings.ini"/>
		<Item Name="TEST.vi" Type="VI" URL="../TEST.vi"/>
		<Item Name="manager.vi" Type="VI" URL="../manager.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="/&lt;vilib&gt;/numeric/LVNumericRepresentation.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
