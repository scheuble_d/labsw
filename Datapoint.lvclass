﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="18008000">
	<Property Name="NI.Lib.Icon" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">402685952</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"V[5F.31QU+!!.-6E.$4%*76Q!!'3A!!!23!!!!)!!!'1A!!!!7!!!!!2&amp;%982B='^J&lt;H1O&lt;(:D&lt;'&amp;T=Q!!!!!!I"A!A!!!-!!!+!!!!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!"AH9Q%99=(1[6I8[\R1_(^!!!!$!!!!"!!!!!!J[/`&gt;@X\/U/#7IE"S?U'/^1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!':^!%]\/G*'BWQ=,T&amp;[]J1"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1"WETDB.Y.L'+6$-)&gt;8M_TQ!!!!1!!!!!!!!!@Q!"4&amp;:$1Q!!!!)!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!F:*1U-!!!!!!1V%982B6(FQ:8-O9X2M5&amp;2)-!!!!"1!!1!$!!!.2'&amp;U962Z='6T,G.U&lt;!!!!!)!!@]!!!!"!!%!!!!!!!)!!!!!!!!!!!!!!!!!!!!!!!-!!!!!!A!#!!!!!!!G!!!!(HC=9_"A9'ZAO-!!R)Q-4!V=1";4"J$XA9'"1Y!"!',!"DM!!!!!!"1!!!!/?*RD9'8A:O#!1A9!!9A!-1!!!%E!!!%9?*RD9-!%`Y%!3$%S-$$T!7F7.(%Q$7.4%_!S&amp;Z&gt;&gt;5(&amp;G)':"=C&gt;1D'E0E'9#C506+%'EG.Y!]1FU=`CB^!5E-1$;G#B]!!!!!!!!$!!"6EF%5Q!!!!!!!Q!!!9)!!!.!?*QL9'2AS$3W-$M!J*G"7)+BA3%Z0S76FQ():Y#!.UQ-&amp;)-!K(F;;/+'"Q[H!9%?PXQ,G.`NIM,38+0#QV4+^\^%B30A"5CQ_1D(Y7[0H///.G!F(&amp;E-71Q"`Q-TGI`QA(5D[=^C!'I4!7'I4BYU]XV57!Q0.&amp;1K-Z1+(W]U993Y)B#KG/5Q1@V!^Q(.Y$DYE+6X)FCA%U2WBD"+((&gt;BV"%$MHNZ'!-2\E:T8VAXU&amp;&gt;:D'%Q_7[WYQY;)0:R"R%)F1'B+C"5!9D;!3,C$G-,V\7P\_VC"&gt;*M3')/5.Q!R+"YB7%^"E9'E)?:A,!$30`Z``_`$6#%#3KG#"5$M7^#W2J)?H\#Z?XB9N&amp;1-59EM&gt;F1-1=E^Y$M!.GU&amp;UBL1.G(I?Q'K0N"9KR!1S:!W4R!&gt;A'5,1RE&lt;Y#SJ9"M!3B&lt;%=D_!'7L1&gt;E(I,'+4DP\O\ACBRUI8=03O!11*_=7'"DI61@L"/OEBNO9W/G%!;&amp;P3+UY3+YAO1S,&amp;!-!@R32@!!!!!!"2!!!!B2YH(.A9'$).,9Q;W"E9'!'9AG'"I&lt;E`*25"C4QA*%"*QA0;X\$U6WD)N&amp;&gt;IC,37;,#UGGD]N`]P^E,E'4T%9\/AY9(GH]S]E]ZS._["#D5[]&lt;3[;,#UOP/UMGC]L_=Z8]:#[&lt;3UNJOE"I0FEY@&amp;::O(J8?1*:/%28_\1%MBA@3L!.:1%KC)%K9Y%K9A%L_&amp;T,^,W"#.L$V*%CR,D&lt;T`B?S`#^AQ6"=QN&amp;R!MC0CT`=(1,5Z!2W&lt;\?*3K]@3S?03H?:A(721!H1);`FOQ-ZL!-YQ'TB\E!7[Q!7-*O^G^/;AX`+!@ZN"VZPQRVS%,$W^&lt;V&gt;4%!;/9A&gt;A&amp;A*'"-A=2#7"/)`````"\&amp;&gt;I/RCI![9`&amp;1E_5.1NA05,'&gt;`&amp;V@UO!4:*1(%S&lt;E&amp;"A:[V=%[Q4KJY49G&gt;DJB1/A&lt;5AM!+&amp;S)PQ!!!2)!!!(5?*RT9'"AS$3W-(.A:'"A"G)*BA;'Z0S56!9EM)#2!3=)$WN_Q^&amp;&gt;IS,28;)CUFGCQN*JIR,_X_Q&amp;3+\Z#%@L!@\7\5"GLRN,JYM+3`.RC(Q:#\+#;5"G.V#WVY/FUU?&amp;J:N(J4?1J6.%B8_\!UPT$_;U8=Q-;@R/&amp;^)[!VH!SA+9/E/!:BVE[_22#@R@Q)1QK^10V:TGASQ1.5DWF7JBM_M4PY!$#Z$QY0H%,_A*)BR:$!]!.=4&amp;(]&lt;N?Q;'N;`P\7)#UMB"Z!$%75!2E$A)3Q$RH````Y09FF"W#:*]+Z,]3CD&lt;!7K7M\_,+XJ=A/Q3"_,EAO1SP?JAH7#&gt;V(!&lt;%TO&gt;-#$U$;FF!!"*DGM)!!!!!!!-'!#!"Q!!"$%Y,D!!!!!!$"A!A!!!!!1R/#YQ!!!!!!Q9!)!(!!!%-4AO-!!!!!!-'!#!!!!!"$%Y,D!!!!!!$"A!A!=!!!1R/#YQ!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9"A!!'"G!!"BA9!!:A"A!'A!%!"M!$!!;Q$1!'D$M!"I06!!;!KQ!'A.5!"I#L!!;!V1!'A+M!"I$6!!:ALA!''.A!"A&lt;A!!9"A!!(`````!!!%!0```````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!(BY!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!(CMKKOM?!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!(CMKK/DI[/LL(A!!!!!!!!!!!!!!!!!!!!!``]!!(CMKK/DI[/DI[/DK[RY!!!!!!!!!!!!!!!!!!$``Q#LKK/DI[/DI[/DI[/DI[OM!!!!!!!!!!!!!!!!!0``!+KKI[/DI[/DI[/DI[/D`KM!!!!!!!!!!!!!!!!!``]!KKOLKK/DI[/DI[/D`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[KDI[/D`P\_`P[K!!!!!!!!!!!!!!!!!0``!+KLK[OLK[OKL0\_`P\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OLK[P_`P\_`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[OLK`\_`P\_`P[K!!!!!!!!!!!!!!!!!0``!+KLK[OLK[OL`P\_`P\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OLK[P_`P\_`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[OLK`\_`P\_`P[K!!!!!!!!!!!!!!!!!0``!+OLK[OLK[OL`P\_`P\_K[M!!!!!!!!!!!!!!!!!``]!!+3KK[OLK[P_`P\_K[SE!!!!!!!!!!!!!!!!!!$``Q!!!!#EK[OLK`\_K[OE!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!J+OLK[OD!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!+3D!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!"X!!&amp;'5%B1!!!!!1!#6%2$1Q!!!!%.2'&amp;U962Z='6T,G.U&lt;&amp;"53$!!!!!5!!%!!Q!!$52B&gt;'&amp;5?8"F=SZD&gt;'Q!!!!#!!$`!!!!!1!"!!!!!!!#!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!"\5&amp;2)-!!!!!!!!!!!!!-!!!!'5!!!%DBYH,V97WQ562D_T_RU/\P&gt;WNF#I1MU/[WT#W+,$:2L!!E&gt;*"1I9FMMBA!,OU#VN\4&lt;"E-%V,6)$#%25G)6^+HYBAE0P'!U:%'449$AB1AG#TTY1LR&amp;I1US8@^T:G:H:L?84&gt;/Q$S?4[8]Z``&gt;^Z``0&amp;+#Y2CTF2O#Q#E2]B!_&lt;6(#(%Q1A8C7!`APWA&gt;B%`A-SX5&gt;57#-UC8?Z%4*("5]Y%23KF?0Q&amp;VKH\K5_*G?YW_*^.(7+0ATG6K%IH*DJL:/4IHRBDHQ]TYDKB4,R""HBGG8`E(!CVI%*)4;0LNYK-A*%+?@Z7-7LI@:)4+:P867#DY6UK3!KC=)O/4E8)W,K+SSEIY9-/N9:)1&amp;$TI/,&amp;S_;4F\.+=CWM1R^S#!!O?3I'=?H7%G5&gt;=P*3O&lt;D:D[9ZZ+22`'0H+*\JU[:LN/5B)CO[(&gt;1,ZH['H\K'O&lt;XY-%$^-.6^TOAQH1ZO6&lt;Q#@@$&gt;`)+N]9O!1%3XS7ETK17E7&amp;(D&lt;??MM"MCZ')Q!IC.O"TAQLT9QFO.`"'LJ.QDN(!'T3M2"L)+YQ'FX*="*/(5R0QI0$RCL,;VJ\O;+2,[NAH\7U.&gt;8&gt;,H6UNP;&amp;I2!K(IK&amp;MBF9J#&gt;&gt;37DV.RM1"*=#4DW$!CH9($!Y/)A#YGK[LU87GH%T\?@6K$-4$*O)UKYH=SYC=-NSYH[)87-%:GH5ST:ZG[_^M0528B],7,^G&lt;5F02#V(28D&amp;TUT:&amp;,ZZ[23^"I2X/5$4UEVYY0YY[FWJ/&amp;E8X)F&lt;(I(]=HW8I=]3G[&amp;\-=]T)-Z[CFW=LON@)F6:U@X_`T1^Z7:&amp;7.%_)JOD9T&gt;44V&amp;/K[`/J*,E.XT"&gt;;Q!HE9ZQAG0^BV2CEAB]S0$0.`$@/KK5#Z#!T4F)W&gt;X9UB;2'K+BNMY-(O[J]&amp;IYQ&lt;M7SEGX4&lt;B&amp;M"DW'97GP+T1*U_?9+'Y;L&amp;DZ8#5ZW)S@?/KCE=,!CM$#R9VOQ-(6T@KY4UK.+#W-4L(IF?$,G_1Q)-2,V__D"&amp;R^@KR7E\UE2'(&amp;0&gt;LYCJ#%YG:F..6L.$:WCN5U0$:JJ3)=LL;46V_=T?.WGY=&lt;$=3I&lt;O2U0_FS?^G4_[\W70&lt;$=KL+?P=&amp;]"S?(/#=\^&amp;B7XBB.-&amp;3N,@9.'$&amp;^[#_&lt;JQ/+0A:ER2,#&gt;,D"4UZY-SK^R:N&lt;S^"'IC-:.SOBIFK+.5/Z:J+(@4X&lt;G&lt;\L*BC%&gt;N/R[VQ(9)P!(+=-H87C.U9#0MWUFDG(9\,(:.HWBW0.I6OT$SX,A`4YUP($AK3_"W&amp;&lt;PV6U*2Z#OAL^R[H#P90&gt;-8B?NY5#OR'VBHDO0'G$.('P_A3BRIBT6P7[CV*\.@9O1@./E7W)[J%X&lt;!;30Q9XM&lt;-U^J$9_H&amp;.^9WK`DRQTN4=.U/]U_/D2K3TSA/H[SY/DZ1]-R,^DH-"&amp;3UABV9MTLG1D6D9H1CR-AZ.12SK`N;)^W&gt;&lt;2G9\2R;D(;.%G-.I_#E&gt;0%+)TT7%Z)./*[D.A*/RB#$K0:PW-AJ&amp;TQV^G\`;I=OLWAY0UE_H:HJI:?6_%)^HLAM7`5U:"&amp;_.=8^/4J3@-OH@4V.HJ3@W.O$K:0//LF#MC?,_^B4F_&gt;-6_U9P)2RF*ITZAPNW\&gt;'HO_R++""&gt;8\^,#&amp;+LSP5?WR5*W(&amp;=W!ZBSC]OGJN7"/@HV07[3L:;_T)&gt;L6UL[@2`1C_7M\/FIDI8:T=-1QX2,\'+-`T]3$QZ0\'00E/M9_M)YR&lt;4@0I@_MS?^GF-9_VG[SRFB@RD%B+*&gt;:K#P\''-]W+_PRSTHJ$'CH:0]9"]8F#"9HAJ7A0=B6A#OBWE8%L;Y&amp;'\58!2MY\_&gt;V'WWYJ78&lt;G)W:FI09@MV^J]R?Y_=9_^R&lt;OFKW&gt;`3HC(T!B8_62)_P096;:%&amp;'LE5?*$"D&lt;8`=O&gt;8J+3H\QNTVF*EBL\8-'@0XVG?LVG?LVL!RDS0N$S^7JZP^2M\T'.Z--/5Z%'I(G?XUJEMT_Y=&lt;I8J]W7+:'C58FK*`%TYD4.M&amp;=H6Q#&amp;/)^VF^F/E`$2N5X_CX_R-SA@'J0TZ#8NJL+)_V*&lt;:2J'$4Z]2VZ]^)[\04D(8ZS&lt;,^??D=_VG8,PK,2^ZQNVL0?H`V1A.MJ);TU"=B`'0K&amp;#CV"&lt;@%$=A8BZBA\"/0)M@Q0AN0%-Y+TS+`ZT_&lt;UZ]5^IT&gt;J.5=VPE!&lt;^1_D]/A3U6!!!!"!!!!&amp;M!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!%&gt;1!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!ZRA!A!!!!!!"!!A!-0````]!!1!!!!!!SQ!!!!=!$E!Q`````Q2/97VF!!!11$$`````"E^S;7&gt;J&lt;A!!71$R!!!!!!!!!!)22'&amp;U98"P;7ZU,GRW9WRB=X-.2'&amp;U962Z='6T,G.U&lt;!!R1"9!"!&gt;/&gt;7VF=GFD"F.U=GFO:Q2%982F"U*P&lt;WRF97Y!#%2B&gt;'&amp;U?8"F!!!-1&amp;-(1W^O&gt;(*P&lt;!!+1&amp;-&amp;6G&amp;M&gt;75!%E"5!!9+6'FN:3"4&gt;'&amp;N=!!!*%"1!!9!!!!"!!)!!Q!%!!522'&amp;U98"P;7ZU,GRW9WRB=X-!!1!'!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!$U9!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!9!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E9!)!!!!!!!1!&amp;!!=!!!%!!.\^?'Y!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'2A!A!!!!!!"!!5!"Q!!!1!!XPVY&lt;A!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!$H'!#!!!!!!!%!#!!Q`````Q!"!!!!!!$,!!!!"Q!/1$$`````"%ZB&lt;75!!""!-0````]'4X*J:WFO!!":!0%!!!!!!!!!!B&amp;%982B='^J&lt;H1O&lt;(:D&lt;'&amp;T=QV%982B6(FQ:8-O9X2M!$&amp;!&amp;A!%"UZV&lt;76S;7-'5X2S;7ZH"%2B&gt;'5(1G^P&lt;'6B&lt;A!)2'&amp;U982Z='5!!!R!5Q&gt;$&lt;WZU=G^M!!J!5Q6797RV:1!31&amp;1!"AJ5;7VF)&amp;.U97VQ!!!E1&amp;!!"A!!!!%!!A!$!!1!"2&amp;%982B='^J&lt;H1O&lt;(:D&lt;'&amp;T=Q!"!!9!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E9!)!!!!!!!1!&amp;!!-!!!%!!!!!!#)!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!"&amp;2A!A!!!!!!(!!Z!-0````]%4G&amp;N:1!!%%!Q`````Q:0=GFH;7Y!!&amp;E!]1!!!!!!!!!#%52B&gt;'&amp;Q&lt;WFO&gt;#ZM&gt;G.M98.T$52B&gt;'&amp;5?8"F=SZD&gt;'Q!-5!7!!1(4H6N:8*J9Q:4&gt;(*J&lt;G=%2'&amp;U:1&gt;#&lt;W^M:7&amp;O!!B%982B&gt;(FQ:1!!$%"4"U.P&lt;H2S&lt;WQ!#E"4"6:B&lt;(6F!"*!6!!'#F2J&lt;75A5X2B&lt;8!!!#2!5!!'!!!!!1!#!!-!"!!&amp;%52B&gt;'&amp;Q&lt;WFO&gt;#ZM&gt;G.M98.T!!%!"A!!!!!!!!!!!!!9!)!!!!!!!1!%!!!!!1!!!!!!!"A!A!!!!!!"!!1!!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!!I!%!!!!!1!!!06!!!!+!!!!!)!!!1!!!!!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!''!!!$"XC=F:(04M*!'-1(WF*!5%$&amp;PW$2MQ3C$^#)]9D%%B-4$T:13*03EL)1P0GE0I-]A.(:FKC*C=,/I@NN^^P^T3S!5TRAA8AEC^?WM-?"[YO[._NZ^G33FSP&gt;Z\%TK@?%B[::BKKXJS-H&gt;(MJ3Y3O0V3ZR&gt;'PAM"T&lt;"^JW3$9!'S;D1]/N7W07"8C+H5&lt;OE08"X+GJ&lt;=#8Y3"B[RJ;@?W.X61-LN):&lt;POS$%M99`'Q#-7L_^T`Q_]?%8C8:I&gt;J*"!ELM6K.!K,7][%5ZI"!-D;D$'I4MDL^&amp;H)T1+7@3"R"NU&amp;MKSR!5SS*A[F)%X2.J]59)\%3]YQ9"?/NS[A2TSSL1`1"K2.T48C$*$5C*;`.!SSB([&amp;F6!E3IJ9NYH4)&gt;G.L(.75]?3X0H[\R"YSO2]IK*-$N.IGHMIL%H3-I&lt;=BD9RQ&amp;4EDK5]3SF,`6\Z@P05;1YWA3/@W#N^F"K2&amp;($'@\T4E4Q[$1PW]%O^F""&amp;3@3#S`3/+N3"G=K[VI=[3=&lt;#;#8!!!!!!"F!!%!!A!$!!1!!!")!!]%!!!!!!]!W1$5!!!!51!0"!!!!!!0!.E!V!!!!&amp;I!$Q1!!!!!$Q$:!.1!!!"DA!#%!)!!!!]!W1$5#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4!!!!"35V*$$1I!!UR71U.-1F:8!!!:+!!!"&amp;)!!!!A!!!:#!!!!!!!!!!!!!!!)!!!!$1!!!2%!!!!'UR*1EY!!!!!!!!"6%R75V)!!!!!!!!";&amp;*55U=!!!!!!!!"@%.$5V1!!!!!!!!"E%R*&gt;GE!!!!!!!!"J%.04F!!!!!!!!!"O&amp;2./$!!!!!"!!!"T%2'2&amp;-!!!!!!!!"^%R*:(-!!!!!!!!##&amp;:*1U1!!!!#!!!#((:F=H-!!!!%!!!#7&amp;.$5V)!!!!!!!!#P%&gt;$5&amp;)!!!!!!!!#U%F$4UY!!!!!!!!#Z'FD&lt;$A!!!!!!!!#_%R*:H!!!!!!!!!$$%:13')!!!!!!!!$)%:15U5!!!!!!!!$.&amp;:12&amp;!!!!!!!!!$3%R*9G1!!!!!!!!$8%*%3')!!!!!!!!$=%*%5U5!!!!!!!!$B&amp;:*6&amp;-!!!!!!!!$G%253&amp;!!!!!!!!!$L%V6351!!!!!!!!$Q%B*5V1!!!!!!!!$V&amp;:$6&amp;!!!!!!!!!$[%:515)!!!!!!!!$`!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(!!!!!!!!!!!`````Q!!!!!!!!$!!!!!!!!!!!$`````!!!!!!!!!.1!!!!!!!!!!0````]!!!!!!!!!X!!!!!!!!!!!`````Q!!!!!!!!&amp;A!!!!!!!!!!$`````!!!!!!!!!7A!!!!!!!!!!P````]!!!!!!!!"F!!!!!!!!!!!`````Q!!!!!!!!'M!!!!!!!!!!$`````!!!!!!!!!@Q!!!!!!!!!!0````]!!!!!!!!#$!!!!!!!!!!"`````Q!!!!!!!!/5!!!!!!!!!!,`````!!!!!!!!".Q!!!!!!!!!"0````]!!!!!!!!&amp;^!!!!!!!!!!(`````Q!!!!!!!!9%!!!!!!!!!!D`````!!!!!!!!"B1!!!!!!!!!#@````]!!!!!!!!'*!!!!!!!!!!+`````Q!!!!!!!!9U!!!!!!!!!!$`````!!!!!!!!"E1!!!!!!!!!!0````]!!!!!!!!'8!!!!!!!!!!!`````Q!!!!!!!!:Q!!!!!!!!!!$`````!!!!!!!!"P1!!!!!!!!!!0````]!!!!!!!!+_!!!!!!!!!!!`````Q!!!!!!!!NU!!!!!!!!!!$`````!!!!!!!!%=A!!!!!!!!!!0````]!!!!!!!!2U!!!!!!!!!!!`````Q!!!!!!!"(9!!!!!!!!!!$`````!!!!!!!!%?A!!!!!!!!!!0````]!!!!!!!!35!!!!!!!!!!!`````Q!!!!!!!"*9!!!!!!!!!!$`````!!!!!!!!&amp;N1!!!!!!!!!!0````]!!!!!!!!7X!!!!!!!!!!!`````Q!!!!!!!"&lt;E!!!!!!!!!!$`````!!!!!!!!&amp;R!!!!!!!!!!A0````]!!!!!!!!9H!!!!!!.2'&amp;U98"P;7ZU,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2&amp;%982B='^J&lt;H1O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!!!!!!!Q!"!!!!!!!!!!!!!!%!'%"1!!!22'&amp;U98"P;7ZU,GRW9WRB=X-!!1!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"A!A!!!!!!!!!!!!!!!!!!"!!!!!!!"!!!!!!9!$E!Q`````Q2/97VF!!!11$$`````"E^S;7&gt;J&lt;A!!71$R!!!!!!!!!!)22'&amp;U98"P;7ZU,GRW9WRB=X-.2'&amp;U962Z='6T,G.U&lt;!!R1"9!"!&gt;/&gt;7VF=GFD"F.U=GFO:Q2%982F"U*P&lt;WRF97Y!#%2B&gt;'&amp;U?8"F!!!-1&amp;-(1W^O&gt;(*P&lt;!!+1&amp;-&amp;6G&amp;M&gt;75!7A$RXPVV;A!!!!)22'&amp;U98"P;7ZU,GRW9WRB=X-.2'&amp;U98"P;7ZU,G.U&lt;!!S1&amp;!!"1!!!!%!!A!$!!1&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!&amp;!!!!"@``````````````````````````!!!!!!!!!!!!!"A!A!!!!!!"!!1!!!!"!!!!!!!!'!#!!!!!!!%!"!!!!!%!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"A!A!!!!!!!!!!!!!!!!!!"!!!!!!!#!!!!!!=!$E!Q`````Q2/97VF!!!11$$`````"E^S;7&gt;J&lt;A!!71$R!!!!!!!!!!)22'&amp;U98"P;7ZU,GRW9WRB=X-.2'&amp;U962Z='6T,G.U&lt;!!R1"9!"!&gt;/&gt;7VF=GFD"F.U=GFO:Q2%982F"U*P&lt;WRF97Y!#%2B&gt;'&amp;U?8"F!!!-1&amp;-(1W^O&gt;(*P&lt;!!+1&amp;-&amp;6G&amp;M&gt;75!%E"5!!9+6'FN:3"4&gt;'&amp;N=!!!8!$RXPVY&lt;A!!!!)22'&amp;U98"P;7ZU,GRW9WRB=X-.2'&amp;U98"P;7ZU,G.U&lt;!!U1&amp;!!"A!!!!%!!A!$!!1!"2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!9!!!!'!!!!!!!!!!%!!!!#!!!!!Q!!!!4`````!!!!!!!!!!!!!"A!A!!!!!!"!!1!!!!"!!!!!!!!'!#!!!!!!!%!"!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!9!)!!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="Datapoint.ctl" Type="Class Private Data" URL="Datapoint.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Name" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">Name</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Name</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="DATAPOINT Read Name.vi" Type="VI" URL="../DATAPOINT Read Name.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!-0````]%4G&amp;N:1!!+E"Q!"Y!!"-22'&amp;U98"P;7ZU,GRW9WRB=X-!$52B&gt;'&amp;Q&lt;WFO&gt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!K1(!!(A!!%R&amp;%982B='^J&lt;H1O&lt;(:D&lt;'&amp;T=Q!-2'&amp;U98"P;7ZU)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="DATAPOINT Write Name.vi" Type="VI" URL="../DATAPOINT Write Name.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%52B&gt;'&amp;Q&lt;WFO&gt;#ZM&gt;G.M98.T!!V%982B='^J&lt;H1A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$E!Q`````Q2/97VF!!!K1(!!(A!!%R&amp;%982B='^J&lt;H1O&lt;(:D&lt;'&amp;T=Q!-2'&amp;U98"P;7ZU)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!)1!!!!EA!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="Origin" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">Origin</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Origin</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="DATAPOINT Read Origin.vi" Type="VI" URL="../DATAPOINT Read Origin.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%B!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'4X*J:WFO!!!K1(!!(A!!%R&amp;%982B='^J&lt;H1O&lt;(:D&lt;'&amp;T=Q!.2'&amp;U98"P;7ZU)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#J!=!!?!!!4%52B&gt;'&amp;Q&lt;WFO&gt;#ZM&gt;G.M98.T!!R%982B='^J&lt;H1A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="DATAPOINT Write Origin.vi" Type="VI" URL="../DATAPOINT Write Origin.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%B!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%52B&gt;'&amp;Q&lt;WFO&gt;#ZM&gt;G.M98.T!!V%982B='^J&lt;H1A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%%!Q`````Q:0=GFH;7Y!!#J!=!!?!!!4%52B&gt;'&amp;Q&lt;WFO&gt;#ZM&gt;G.M98.T!!R%982B='^J&lt;H1A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!B!!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="Time Stamp" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">Time Stamp</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Time Stamp</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="Read Time Stamp.vi" Type="VI" URL="../Read Time Stamp.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%D!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!6!!'#F2J&lt;75A5X2B&lt;8!!!#J!=!!?!!!4%52B&gt;'&amp;Q&lt;WFO&gt;#ZM&gt;G.M98.T!!V%982B='^J&lt;H1A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+E"Q!"Y!!"-22'&amp;U98"P;7ZU,GRW9WRB=X-!$%2B&gt;'&amp;Q&lt;WFO&gt;#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="Write Time Stamp.vi" Type="VI" URL="../Write Time Stamp.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%D!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%52B&gt;'&amp;Q&lt;WFO&gt;#ZM&gt;G.M98.T!!V%982B='^J&lt;H1A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%E"5!!9+6'FN:3"4&gt;'&amp;N=!!!+E"Q!"Y!!"-22'&amp;U98"P;7ZU,GRW9WRB=X-!$%2B&gt;'&amp;Q&lt;WFO&gt;#"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!*)!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="Value" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">Value</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Value</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="Read Value.vi" Type="VI" URL="../Read Value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&lt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!J!5Q6797RV:1!K1(!!(A!!%R&amp;%982B='^J&lt;H1O&lt;(:D&lt;'&amp;T=Q!.2'&amp;U98"P;7ZU)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#J!=!!?!!!4%52B&gt;'&amp;Q&lt;WFO&gt;#ZM&gt;G.M98.T!!R%982B='^J&lt;H1A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="Write Value.vi" Type="VI" URL="../Write Value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&lt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%52B&gt;'&amp;Q&lt;WFO&gt;#ZM&gt;G.M98.T!!V%982B='^J&lt;H1A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!#E"4"6:B&lt;(6F!#J!=!!?!!!4%52B&gt;'&amp;Q&lt;WFO&gt;#ZM&gt;G.M98.T!!R%982B='^J&lt;H1A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="createDatapoint.vi" Type="VI" URL="../createDatapoint.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;S!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%52B&gt;'&amp;Q&lt;WFO&gt;#ZM&gt;G.M98.T!!V%982B='^J&lt;H1A&lt;X6U!#2!=!!?!!!1$E2F&gt;GFD:3ZM&gt;G.M98.T!!!*2'6W;7.F)'FO!":!5!!$!!!!!1!##'6S=G^S)'FO!!!+1&amp;-&amp;&gt;G&amp;M&gt;75!71$R!!!!!!!!!!)22'&amp;U98"P;7ZU,GRW9WRB=X-.2'&amp;U962Z='6T,G.U&lt;!!R1"9!"!&gt;/&gt;7VF=GFD"F.U=GFO:Q2%982F"U*P&lt;WRF97Y!#%2B&gt;'&amp;U?8"F!!!/1$$`````"%ZB&lt;75!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"A!(!!A!#1!+!Q!!?!!!#1!!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!A!!!!)!!!!#!!!!!A!!!))!!!!!!%!#Q!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="DataTypes.ctl" Type="VI" URL="../DataTypes.ctl">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"&gt;!!!!!1"6!0%!!!!!!!!!!B&amp;%982B='^J&lt;H1O&lt;(:D&lt;'&amp;T=QF$&lt;WZU=G^M)$%!-5!7!!1(4H6N:8*J9Q:4&gt;(*J&lt;G=%2'&amp;U:1&gt;#&lt;W^M:7&amp;O!!B%982B&gt;(FQ:1!!!1!!!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
	</Item>
</LVClass>
