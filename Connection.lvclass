﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="18008000">
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">402685952</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.8</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"_05F.31QU+!!.-6E.$4%*76Q!!'TQ!!!24!!!!)!!!'RQ!!!!8!!!!!2*$&lt;WZO:7.U;7^O,GRW9WRB=X-!!!!!I"A!A!!!-!!!+!!!!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!"Z.IY,XE/*2[_5,@&gt;^G9H8!!!!$!!!!"!!!!!!-!Q4*21S1E7BD-`U[4V=1N1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!)#DC`MLFH60D*!^0/M9+DY"!!!!`````^1&gt;D.G0!,)%[9!*G/TY1HY!!!!1',)H7M?5'4"+#=['_A,H&amp;Q!!!!1!!!!!!!!!?A!"4&amp;:$1Q!!!!)!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!F:*1U-!!!!!!2.$&lt;WZO:7.U;7^O6(FQ:8-O9X2M5&amp;2)-!!!!#%!!1!%!!!'3'6M='6S%U.P&lt;GZF9X2J&lt;WZ5?8"F=SZD&gt;'Q!!!!!!!&amp;#!!!!!!!$!!!!!!!#!!)!!!!!!#A!!!!C?*RDY'2A&lt;G#YQ!$%D!Z-$6R!&amp;F-'!ZBG_-$!Q"(!!!#.OQB7!!!!&amp;!!!!""YH'.A9_"BY)"$"A!#(A!\!!!!3Q!!!2BYH'.AQ!4`A1")-4)Q-'M!;49U=4!.9V-4Y$)8FVV1=79A:A&amp;C6JAQ!Q04(C$.""+(KN'(3$(^!/)4[/&lt;Q1_E,3')!_5!IM!!!!!!-!!&amp;73524!!!!!!!$!!!"CA!!!VBYH#NA:'$).,9Q/Q#EG9&amp;9AK'")4E`*:78!=BHA)!X4!Q5AQ#I?6JIYI9($K="A2[`@!O9X_WCQN*=I],$6-LXPU3&amp;)_!&amp;3,$Z#-@B&lt;I_=YYYW9#5=71R:$!(`!T/;D`#!&gt;30JTW)!;B-"9;B/(D4T@629$!]U6#ITF!I@&lt;T2BB,AC%+K9Z4""`5$X!=XA/0C1J8=C7+!42(;'-%I=&gt;W(5%1/S?XE9!R(O2H.@7$@16VG-94$Z&lt;L&lt;D$BIA^H%(%1C6!;%K)&amp;1"C.I")O)/9QP8N;`P\7)&amp;UGR)9AZ1X!$%I(C&amp;94U'2A;1BZG!M!.)``H``\].5)1*+K9)&amp;1/R&lt;U,:D!TW=$WTI7);3/&lt;]B/N"K)O'CDEAO1&gt;E"]CGP5";!]I_$'5X1.U0%G-&amp;'D)"SO9"MAOA&lt;'%A?Q/5,16E#U$:CE$W"SB&lt;$=I_!)V6&gt;.L:X]56/?R![2K7RF7!/$GXQ-"!TU_P/D8=RMR/RUEH7#@."5*!5&amp;&amp;;,3C-EAO3S`!K!KI"!)7XG!1!!!!!!8)!!!+!?*RT9'"AS$3W-'.A9G"A:G2AE'"I9%D/4UFF1!)/4!QY18"9]RO/&lt;B]6C?Y1&amp;:(/%"774BY6`OV'BA@3L!XZJRQOV?VV9OJU57(J&gt;74K:&amp;&amp;JK'*CY*^[K+%32"XMP'"QQO"5C;!*)`_W!S_^A&lt;:XX(DJ!K3[12I]'$N^6&amp;C[?62[!RE\264_&amp;T,_,W"]!&lt;+T_1B(ZU($!]U`'@GH(/2P&amp;9$L9)(L9!(K_-R@Z]$SC6`!ES7.8_^!.2O10&amp;EF!#3"?MY"D?(P/M%!U]E"V]E"NIPD@Q%(.LO7-*"A&amp;Q@)LN+UKB2M&gt;I"U=)"U='$I-+ES)FK((&amp;A(5\89*`Z$"RH3_/M/.$^A@-X5=1,I],DIQ^D-[1\A-$RA(=D"0_8![ZNL8^`&lt;"1Q`"E&lt;E_!:C*7"+!%5\#"=!]:````_$W(?A&lt;!?I7G&gt;`&amp;V@UN!)S3Q7)EX-,$!TU`03K5].NT/RUH(3#&gt;&gt;*=)!1%&amp;;86!A$"P;R#!!!!!!%+!!!#!(C==W"A9-AUND"L9'2A9!:C#99'BO4]F&amp;1'*(#!E1%H#'N_Q^(NIC,3\;-CU/GDQN,*IM+`8&lt;$Z&amp;WP;0F;'.([H4WG&gt;"LW"4*UB+CS^!5S&gt;)CJ&gt;HEQ&gt;(ER!,3S^(IQA(&gt;U]+I[&gt;0#K"`QM98Y"-&lt;$\#U8K!PX5(E!F2R1*6V2P)!N4`C6`!A?54P[!D#U37!SL&lt;@*!$9AI(QJ43-F*-[!XEA-JSA'1Z3*@N&gt;O$I&gt;/1Q0!#U0/YQ\D"$"GN@X^M&amp;#F\E)(9!YCSA#"/1"G%`)0\T``^`%(M+F/U!6?PM\_++(F=AMR3"/,EAO5SP/D8=RMR/RUEH7#@."5*!5&amp;&amp;;,6!.!,Q]&lt;T]!!!!!!!Q9!)!(!!!%-4AO-!!!!!!-'!#!!!!!"$%Y,D!!!!!!$"A!A!=!!!1R/#YQ!!!!!!Q9!)!!!!!%-4AO-!!!!!!-'!#!"Q!!"$%Y,D!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A'!!!9'9!!''"A!"G!'!!;!!1!'Q!-!"L!.!!;-/Q!'A^5!"I#L!!;!V1!'A+M!"I$6!!;!KQ!'A.5!"G#O!!99W!!'"O!!"A'!!!@````]!!!1!````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!#QM!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!#[V@C;U,!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!#[V@.45V.9GN#Q!!!!!!!!!!!!!!!!!!!!$``Q!!#[V@.45V.45V.47*L1M!!!!!!!!!!!!!!!!!!0``!)F@.45V.45V.45V.45VC;U!!!!!!!!!!!!!!!!!``]!8V]V.45V.45V.45V.48_C1!!!!!!!!!!!!!!!!$``Q"@C9F@.45V.45V.48_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*8T5V.48_`P\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9G*C6_N`P\_`P\_8Q!!!!!!!!!!!!!!!!$``Q"@C9G*C9G*C@\_`P\_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*C9G*`P\_`P\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9G*C9H_`P\_`P\_8Q!!!!!!!!!!!!!!!!$``Q"@C9G*C9G*C@\_`P\_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*C9G*`P\_`P\_`F]!!!!!!!!!!!!!!!!!``]!C9G*C9G*C9H_`P\_`P[*C1!!!!!!!!!!!!!!!!$``Q!!8V_*C9G*C@\_`P[*L6]!!!!!!!!!!!!!!!!!!0``!!!!!&amp;_*C9G*`P[*C6]!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!"@C9G*C45!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!8T5!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!()!!5:13&amp;!!!!!"!!*52%.$!!!!!2.$&lt;WZO:7.U;7^O6(FQ:8-O9X2M5&amp;2)-!!!!#%!!1!%!!!'3'6M='6S%U.P&lt;GZF9X2J&lt;WZ5?8"F=SZD&gt;'Q!!!!!!!"#!!!!!!!!!!%!!!"V5&amp;2)-!!!!!!!!!!!!!-!!!!!"U!!!"N9?*T.G6NM&amp;&amp;59R\]TOZ4:8OQMN+8FNB?H3Y%7'B+O%3VUKK&amp;!77A&lt;%"(9&gt;B&gt;I5LJ.O[U3Z?L3B#CA%*MI)@%'0P,!ATSA)CFI8"5EU51QW5+-,Z#)$Q1#4-@PH.GZ\7\&lt;J8*J(UYHW_^STP@^TH`/HA*-;B#+O1(9*1-2\O$$#BGSAT%#U&amp;@"1_,(VQ.#)XE!J+#%S&amp;$&amp;.QL8O!%S79&lt;=9-T(6UI(Y$:;+`X+%7[O&lt;&lt;RQ(5WTB")-FCV$@D!WQ6ELRA8RV'4RQ"ANKB/G#!@*!,&gt;/&gt;.XF$U&lt;$G"#C:82U6J!")*,(&lt;I^[`9'W5&amp;3EHTIK_")7UC'$)-8S/M4Y.)S)K=_TE,:W4L,NV5)#BCS$U[&gt;0'UZ/V=H(JL%!@4A*A'OUN1`B-U[+4?E5Y_8-*ZPZ9*Z',9`E'DB+ZU[&gt;EFX(3T%"8&gt;(PT=33K;`G*V=RPRMX&lt;K!@DAG`&lt;4)5C0'F@!F`08C6Z+W/@A5%301S?N@U&lt;?+6M#*CF*H//NI,ZD%/WV'[C!DV_&amp;QPQYRID.M-^E2'UAN(740M7D.?Q';1FVET(.)"!9RO("WG'Z+^TTOFOL7L-R,K=)?XO*N&lt;!ZW&gt;\P;/FOZ!*/1/"C+"V$YNFG+/_&lt;1'."F$"!L"4IZ!C\HG94BZ]C37!5@$^56UH3$'&gt;4]H7]VWP?Z"I_YUKV'`F\"_ULW'L&lt;3'J9MYD6Q()\?,D7@9+,(R'"W*BYX\W8C8DNQ&lt;\+]."OFTE(3HE,Q-#_FT(T`J]R$!85GE1YDMB/YBK*WP/BGEITW11R!;QG="_OQWEYY_)@4J(J\UB3GE5^]EUHN\?SV_W+F&amp;/OFW1D43F9@+1UL[#?5G[9&gt;^D(1(]WD%&gt;EAR.U776''/6KCTMLU_,&gt;M%[V_H4_&lt;$Q?(/;Q[XN97;)SXB.P=S+&lt;56LUER@KY9*RC&gt;2MZ8(M"MG+2&amp;`KMWN;9&lt;60Q&gt;#8[B(!LV;NZ+7UWMSON9F&gt;*8I81^30@S6KE5WZ$CB)5@U1T'MG!J,5-2,K]+FL!SD%W5A&gt;O:NAQZ/)0S$,:YFD`1U2,:E&lt;4]@JH&lt;&amp;9T:(80%?,:F-_?$$R:LCV+=&lt;&amp;(X\^`(2?'IRIV[9)_&gt;CYLU%U&gt;&amp;.&amp;,[`+;NC&lt;#Z-L=&lt;;T20D(-M;C7&lt;L)#:*U-O2DJ\^CR'QN(JQB6S1AE:M,H\8/KWSE54.T0RU&amp;(Q*CL&lt;T(NJ_&amp;24CK#(DF:4B]O9T2[=$;\2:JL.603@-@,:.'5_GS&lt;,&lt;(*E&lt;K]5+]&amp;.EK]W]JR2]5+=T2^8`]4:&gt;06]QG9TBK;A5?Z6KOH9]WT4]ST4=Y8R@0&gt;85U[(T/V,U&gt;U=80[39878?]@%\:I=F6O\LY?\[/;6W_IJYK+(X[U^?P7TB1YWO;O"$&lt;=1\+)EM)E]+.A,-Q#&lt;LY_%WZN;)JUJ;*/"*Y)W5596WD#KU#&lt;0!'VOJ'D&lt;UK!^ZN(1XK_D@2Y`PZ7-^LO$ILVY;,4&gt;7:$!?WGA+^C"2\&amp;5P.^\-HA@(%VYEU/D#7^S_/HD4&gt;Y@)&gt;\EAT2Y:TU#XKPR`%Q4^O*(ZS&amp;I/2/4;9._Y6C1+&gt;H6KV;[`?'/3",:7/9S3ZFZ'LM9\#"#&gt;EK:\8L:@D#6](P4]U84]Q6L/[?L?&lt;IN\&lt;2$'=O$'2Z,(CT7$"8C(..'H=$S&lt;-ZAI^KVD7IA-4-*C@(M%"I=(IFS%R).&amp;UL@YF1KRPJ[&lt;,KGQ4&amp;&gt;UVL2O2@]&amp;EW$4Q@6.(_'H=^:%^I?RC_8;:L@,].H4U47Y002*'PQR7C3.4DR^'5.4IZ1VO$,.,,'0Z+MA5146O&amp;(L5GS"CM'F&lt;7]D/#7P-P]+8I'+Z_/HE(&gt;U^%T706Y^1T])^1T7*V?TRS'HKW3Y9R;&lt;]#/,\&lt;?,(R$O`U+[X;BN&gt;P$(-_9D$GKV9O&amp;5$#JY\C`P]56&amp;9DR1E0!/.R/YX%65Z&gt;D+$I;&lt;4"WL"/=O'/JC9?/_IZN4&gt;X=KGF1I;9Y$G`[`[/;"?S=?A?SFF5O(\3OF4(*5+]`9%P;.2;CC:O:?+C*HC+5/BO,K:+Z[5=D.^88C(2^:V;;!B7N&lt;&amp;]0^'SE&gt;5L9"78I%G.O?EX_$Q9PBB"DT+9R^L;G+.)J6[XV@2E;^PKQT`N=N8Z\V&lt;#D0@F&lt;Q&amp;I:PM&lt;8J7#8YKZ;&gt;HO&amp;@ZU/'[RP\"`J`77&gt;2&gt;35@X%'("2E*'KC&amp;V*@UT(-7V+LP;&lt;6:9W&amp;D6C!NC12O(,FSJ#P[6G67R*B]W4Y#35-J388*#VD=&amp;6&amp;M#[$K,KU^-W;HL5S('TK[H3IPRKK`6HVI9[71#N@%^E7[GA,27T3EN7WRPKF"N-`9_[E)Y,W'HU'2Y2@V%K9DQD0I@`%:X"%1)G_B,-R3T2"@C9C;&amp;;*:EWR3P2FMU3(V%W5A[^LHRN](M8H"?&gt;.8!%Y&lt;BIO/]V8J=N6FVT=&gt;X]@RDH6G3[\_7M8OYT8?\UI+5-:#$59@L=-B6,VO%P#-MS;SS`D;Y4DX!!B-B4RR`E\@&lt;`L`_XK7[&amp;\2C^T^&gt;RPYM=OPPA`K!+$G!!!!!1!!!"W!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!"4Q!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!3E9!)!!!!!!!1!)!$$`````!!%!!!!!!1U!!!!+!&amp;U!]1!!!!!!!!!"%U.P&lt;GZF9X2J&lt;WZ5?8"F=SZD&gt;'Q!15!7!!5'47^E9H6T#5VP:'*V=V2$5!:4:8*J97Q)282I:8*O:81$66.#!!!/1W^O&lt;G6D&gt;'FP&lt;F2Z='5!!!Z!)1F$&lt;WZO:7.U:71!$%!Q`````Q**5!!!%5!+!!N3:7VP&gt;'5A5'^S&gt;!!31$$`````#%.043"1&lt;X*U!!!01!I!#%*B&gt;72S982F!!!01!I!#&amp;.U&lt;X"C;82T!!!.1!I!"F"B=GFU?1!!&amp;%"Q!!5.9W^O&lt;G6D&gt;'FP&lt;C"*2!!M1&amp;!!#1!!!!%!!A!$!!1!"1!'!!=!#"*$&lt;WZO:7.U;7^O,GRW9WRB=X-!!!%!#1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!"*'!#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!*!!!!!!!!!!%!!!!#!!!!!Q!!!!1!!!!&amp;!!!!"A!!!!=!!!!)!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:'!#!!!!!!!%!"1!(!!!"!!$?D`G?!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E9!)!!!!!!!1!&amp;!!=!!!%!!.[0_:Y!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!",2A!A!!!!!!"!!A!-0````]!!1!!!!!"%1!!!!I!91$R!!!!!!!!!!%41W^O&lt;G6D&gt;'FP&lt;F2Z='6T,G.U&lt;!"&amp;1"9!"A:.&lt;W2C&gt;8-*47^E9H6T6%.1"F.F=GFB&lt;!B&amp;&gt;'BF=GZF&gt;!.%16%$66.#!!!/1W^O&lt;G6D&gt;'FP&lt;F2Z='5!!!Z!)1F$&lt;WZO:7.U:71!$%!Q`````Q**5!!!%5!+!!N3:7VP&gt;'5A5'^S&gt;!!31$$`````#%.043"1&lt;X*U!!!01!I!#%*B&gt;72S982F!!!01!I!#&amp;.U&lt;X"C;82T!!!.1!I!"F"B=GFU?1!!&amp;%"Q!!5.9W^O&lt;G6D&gt;'FP&lt;C"*2!!M1&amp;!!#1!!!!%!!A!$!!1!"1!'!!=!#"*$&lt;WZO:7.U;7^O,GRW9WRB=X-!!!%!#1!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'2A!A!!!!!!"!!5!!Q!!!1!!!!!!,Q!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%!!!&amp;)'!#!!!!!!!I!91$R!!!!!!!!!!%41W^O&lt;G6D&gt;'FP&lt;F2Z='6T,G.U&lt;!"&amp;1"9!"A:.&lt;W2C&gt;8-*47^E9H6T6%.1"F.F=GFB&lt;!B&amp;&gt;'BF=GZF&gt;!.%16%$66.#!!!/1W^O&lt;G6D&gt;'FP&lt;F2Z='5!!!Z!)1F$&lt;WZO:7.U:71!$%!Q`````Q**5!!!%5!+!!N3:7VP&gt;'5A5'^S&gt;!!31$$`````#%.043"1&lt;X*U!!!01!I!#%*B&gt;72S982F!!!01!I!#&amp;.U&lt;X"C;82T!!!.1!I!"F"B=GFU?1!!&amp;%"Q!!5.9W^O&lt;G6D&gt;'FP&lt;C"*2!!M1&amp;!!#1!!!!%!!A!$!!1!"1!'!!=!#"*$&lt;WZO:7.U;7^O,GRW9WRB=X-!!!%!#1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!$!!3!!!!"!!!!I-!!!!I!!!!!A!!"!!!!!!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!&lt;Y!!!._?*SF5=N/WU!50&lt;949Q&gt;#)&amp;#AF)@$9RN2)&lt;&amp;A:1AAM5!VB/YR^K3V:$+209FAVT`A$`AUPA'7L/D*/%#L3J6IZ]K?G4.XZJRT,Y".B(B%-9S:FORW2;13W4W`[9G]';E5B`Y=&lt;0N%RJ@^X#WG]V:ANU77B+FTK,[,L#O5&gt;&lt;"X;HVN\Q/4PT^#Q'_Y)UT%G0#XHDH-YQ#9^CM90R.85AEPE*F#P4BU7F^/#A!VJDD\94`/1C6'W\;3P=N%Z5#67TM)MU4&gt;!,._$_6K^-LO(2]ARO0^\&gt;-&gt;T:HV.VX.&gt;"#F9:\`)F6&lt;X@5$O-.#Q)3&amp;%MKQ-1:HO:8W=S5S4X9]@=`L:=G!?LQY6#'TSE#&amp;6$!?/*&gt;ZN&gt;BC'R.Q`4&amp;9H@1&lt;(0_(*=^5!1D:Q24:,%SCBCGL(X@9CL]XQK5;CGL!A;Y3G3IA2N&gt;9UMLLG-%M0OBP'(/7OI[:&amp;.$0./;ZCL1\!XP`W^1&gt;4&lt;DY\F+:U+H]';#2#QQN(6'BBS5M5_%Q6FCXF[C-YE`E\7262V&amp;TAS`N`&amp;-@3\TR'?_L#S7$6//I9A%@]9F@!WN9@S6XO&amp;ZD/"2A5UC*G-GT$?!H$&amp;;XT!!!!!!!:1!"!!)!!Q!%!!!!3!!0"!!!!!!0!.E!V!!!!&amp;%!$Q1!!!!!$Q$:!.1!!!";!!]%!!!!!!]!W1$5!!!!9Y!!B!#!!!!0!.E!V!B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!'TQ!!!24!!!!)!!!'RQ!!!!!!!!!!!!!!#!!!!!U!!!%2!!!!"N-35*/!!!!!!!!!62-6F.3!!!!!!!!!7B36&amp;.(!!!!!!!!!8R$1V.5!!!!!!!!!:"-38:J!!!!!!!!!;2$4UZ1!!!!!!!!!&lt;B544AQ!!!!!1!!!=R%2E24!!!!!!!!!@2-372T!!!!!!!!!AB735.%!!!!!A!!!BRW:8*T!!!!"!!!!FB41V.3!!!!!!!!!LR(1V"3!!!!!!!!!N"*1U^/!!!!!!!!!O2J9WQY!!!!!!!!!PB-37:Q!!!!!!!!!QR'5%BC!!!!!!!!!S"'5&amp;.&amp;!!!!!!!!!T275%21!!!!!!!!!UB-37*E!!!!!!!!!VR#2%BC!!!!!!!!!X"#2&amp;.&amp;!!!!!!!!!Y273624!!!!!!!!!ZB%6%B1!!!!!!!!![R.65F%!!!!!!!!!]")36.5!!!!!!!!!^271V21!!!!!!!!!_B'6%&amp;#!!!!!!!!!`Q!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"Q!!!!!!!!!!0````]!!!!!!!!!Q!!!!!!!!!!!`````Q!!!!!!!!$5!!!!!!!!!!$`````!!!!!!!!!.Q!!!!!!!!!!0````]!!!!!!!!"8!!!!!!!!!!!`````Q!!!!!!!!&amp;E!!!!!!!!!!,`````!!!!!!!!!:!!!!!!!!!!!0````]!!!!!!!!"K!!!!!!!!!!!`````Q!!!!!!!!(Y!!!!!!!!!!$`````!!!!!!!!!AA!!!!!!!!!!@````]!!!!!!!!$G!!!!!!!!!!#`````Q!!!!!!!!51!!!!!!!!!!4`````!!!!!!!!"C!!!!!!!!!!"`````]!!!!!!!!'-!!!!!!!!!!)`````Q!!!!!!!!:!!!!!!!!!!!H`````!!!!!!!!"F!!!!!!!!!!#P````]!!!!!!!!'9!!!!!!!!!!!`````Q!!!!!!!!:Q!!!!!!!!!!$`````!!!!!!!!"IA!!!!!!!!!!0````]!!!!!!!!'H!!!!!!!!!!!`````Q!!!!!!!!=A!!!!!!!!!!$`````!!!!!!!!#S1!!!!!!!!!!0````]!!!!!!!!,H!!!!!!!!!!!`````Q!!!!!!!",A!!!!!!!!!!$`````!!!!!!!!%OA!!!!!!!!!!0````]!!!!!!!!3]!!!!!!!!!!!`````Q!!!!!!!"-!!!!!!!!!!!$`````!!!!!!!!%WA!!!!!!!!!!0````]!!!!!!!!4=!!!!!!!!!!!`````Q!!!!!!!"CQ!!!!!!!!!!$`````!!!!!!!!',A!!!!!!!!!!0````]!!!!!!!!9Q!!!!!!!!!!!`````Q!!!!!!!"DM!!!!!!!!!)$`````!!!!!!!!'L!!!!!!$E.P&lt;GZF9X2J&lt;WYO9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2*$&lt;WZO:7.U;7^O,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!!!#Q!"!!!!!!!!!1!!!!%!(E"1!!!71W^O&lt;G6D&gt;'FP&lt;EFO:G]O&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!9!)!!!!!!!!!!!!!!!!!!!1!!!!!!!1%!!!!#!&amp;-!]1!!!!!!!!!"%U.P&lt;GZF9X2J&lt;WZ5?8"F=SZD&gt;'Q!.U!7!!1'47^E9H6T"F.F=GFB&lt;!B&amp;&gt;'BF=GZF&gt;!.65U)!!!Z$&lt;WZO:7.U;7^O6(FQ:1!!8!$RXHR#W!!!!!)71W^O&lt;G6D&gt;'FP&lt;EFO:G]O&lt;(:D&lt;'&amp;T=R*$&lt;WZO:7.U;7^O37ZG&lt;SZD&gt;'Q!+E"1!!%!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!%!!!!"`````Q!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!9!)!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!#!&amp;-!]1!!!!!!!!!"%U.P&lt;GZF9X2J&lt;WZ5?8"F=SZD&gt;'Q!.U!7!!1'47^E9H6T"F.F=GFB&lt;!B&amp;&gt;'BF=GZF&gt;!.65U)!!!Z$&lt;WZO:7.U;7^O6(FQ:1!!8!$RXHR#W!!!!!)71W^O&lt;G6D&gt;'FP&lt;EFO:G]O&lt;(:D&lt;'&amp;T=R*$&lt;WZO:7.U;7^O37ZG&lt;SZD&gt;'Q!+E"1!!%!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!%!!!!"`````A!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!9!)!!!!!!!!!!!!!!!!!!!1!!!!!!!1!!!!!$!&amp;-!]1!!!!!!!!!"%U.P&lt;GZF9X2J&lt;WZ5?8"F=SZD&gt;'Q!.U!7!!1'47^E9H6T"F.F=GFB&lt;!B&amp;&gt;'BF=GZF&gt;!.65U)!!!Z$&lt;WZO:7.U;7^O6(FQ:1!!$E!B#5.P&lt;GZF9X2F:!"7!0(?D4QZ!!!!!B*$&lt;WZO:7.U;7^O,GRW9WRB=X-/1W^O&lt;G6D&gt;'FP&lt;CZD&gt;'Q!,%"1!!)!!!!"(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!A!!!!)!!!!!`````Q!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!9!)!!!!!!!!!!!!!!!!!!!1!!!!!!!A!!!!!%!&amp;-!]1!!!!!!!!!"%U.P&lt;GZF9X2J&lt;WZ5?8"F=SZD&gt;'Q!.U!7!!1'47^E9H6T"F.F=GFB&lt;!B&amp;&gt;'BF=GZF&gt;!.65U)!!!Z$&lt;WZO:7.U;7^O6(FQ:1!!$E!B#5.P&lt;GZF9X2F:!!11&amp;-+1W^O&lt;G6D&gt;'FP&lt;A!!7!$RXIV&lt;4A!!!!)31W^O&lt;G6D&gt;'FP&lt;CZM&gt;G.M98.T$E.P&lt;GZF9X2J&lt;WYO9X2M!#Z!5!!$!!!!!1!#(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!Q!!!!-!!!!!!!!!!@````]!!!!9!)!!!!!!!1!%!!!!!1!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"A!A!!!!!!!!!!!!!!!!!!"!!!!!!!$!!!!!!1!81$R!!!!!!!!!!%41W^O&lt;G6D&gt;'FP&lt;F2Z='6T,G.U&lt;!""1"9!"1:.&lt;W2C&gt;8-+47^E9H6T)&amp;2$5!:4:8*J97Q)282I:8*O:81$66.#!!Z$&lt;WZO:7.U;7^O6(FQ:1!!$E!B#5.P&lt;GZF9X2F:!!11&amp;-+1W^O&lt;G6D&gt;'FP&lt;A!!7!$RXIW(GQ!!!!)31W^O&lt;G6D&gt;'FP&lt;CZM&gt;G.M98.T$E.P&lt;GZF9X2J&lt;WYO9X2M!#Z!5!!$!!!!!1!#(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!Q!!!!0`````!!!!!1!!!!)!!!!9!)!!!!!!!1!%!!!!!1!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!9!)!!!!!!!!!!!!!!!!!!!1!!!!!!"!!!!!!$!&amp;U!]1!!!!!!!!!"%U.P&lt;GZF9X2J&lt;WZ5?8"F=SZD&gt;'Q!15!7!!5'47^E9H6T#EVP:'*V=S"51V!'5W6S;7&amp;M#%6U;'6S&lt;G6U!V641A!/1W^O&lt;G6D&gt;'FP&lt;F2Z='5!!!Z!)1F$&lt;WZO:7.U:71!6A$RXIW-8Q!!!!)31W^O&lt;G6D&gt;'FP&lt;CZM&gt;G.M98.T$E.P&lt;GZF9X2J&lt;WYO9X2M!#R!5!!#!!!!!2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!)!!!!#!!!!!!!!!!%!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!9!)!!!!!!!!!!!!!!!!!!!1!!!!!!"1!!!!!*!&amp;U!]1!!!!!!!!!"%U.P&lt;GZF9X2J&lt;WZ5?8"F=SZD&gt;'Q!15!7!!5'47^E9H6T#EVP:'*V=S"51V!'5W6S;7&amp;M#%6U;'6S&lt;G6U!V641A!/1W^O&lt;G6D&gt;'FP&lt;F2Z='5!!!Z!)1F$&lt;WZO:7.U:71!$%!Q`````Q**5!!!%5!+!!N3:7VP&gt;'5A5'^S&gt;!!31$$`````#%.043"1&lt;X*U!!!01!I!#%*B&gt;72S982F!!!01!I!#&amp;.U&lt;X"C;82T!!!.1!I!"F"B=GFU?1!!9A$RXI[[GQ!!!!)31W^O&lt;G6D&gt;'FP&lt;CZM&gt;G.M98.T$E.P&lt;GZF9X2J&lt;WYO9X2M!$B!5!!)!!!!!1!#!!-!"!!&amp;!!9!"RV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!A!!!!)!!!!!!!!!!(```````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"A!A!!!!!!!!!!!!!!!!!!"!!!!!!!'!!!!!!I!81$R!!!!!!!!!!%41W^O&lt;G6D&gt;'FP&lt;F2Z='6T,G.U&lt;!""1"9!"1:.&lt;W2C&gt;8-+47^E9H6T)&amp;2$5!:4:8*J97Q)282I:8*O:81$66.#!!Z$&lt;WZO:7.U;7^O6(FQ:1!!$E!B#5.P&lt;GZF9X2F:!!-1$$`````!EF1!!!21!I!#V*F&lt;7^U:3"1&lt;X*U!"*!-0````])1U^.)&amp;"P=H1!!!^!#A!)1G&amp;V:(*B&gt;'5!!!^!#A!)5X2P='*J&gt;(-!!!V!#A!'5'&amp;S;82Z!!!51(!!"1VD&lt;WZO:7.U;7^O)%F%!'1!]&gt;[/OY1!!!!#%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=QZ$&lt;WZO:7.U;7^O,G.U&lt;!![1&amp;!!#1!!!!%!!A!$!!1!"1!'!!=!#"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!E!!!!*!!!!!!!!!!%!!!!#!!!!!Q!!!!1!!!!&amp;!!!!"A!!!!@`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!9!)!!!!!!!!!!!!!!!!!!!1!!!!!!"Q!!!!!+!&amp;U!]1!!!!!!!!!"%U.P&lt;GZF9X2J&lt;WZ5?8"F=SZD&gt;'Q!15!7!!5'47^E9H6T#5VP:'*V=V2$5!:4:8*J97Q)282I:8*O:81$66.#!!!/1W^O&lt;G6D&gt;'FP&lt;F2Z='5!!!Z!)1F$&lt;WZO:7.U:71!$%!Q`````Q**5!!!%5!+!!N3:7VP&gt;'5A5'^S&gt;!!31$$`````#%.043"1&lt;X*U!!!01!I!#%*B&gt;72S982F!!!01!I!#&amp;.U&lt;X"C;82T!!!.1!I!"F"B=GFU?1!!&amp;%"Q!!5.9W^O&lt;G6D&gt;'FP&lt;C"*2!"E!0(?DMN3!!!!!B*$&lt;WZO:7.U;7^O,GRW9WRB=X-/1W^O&lt;G6D&gt;'FP&lt;CZD&gt;'Q!/E"1!!E!!!!"!!)!!Q!%!!5!"A!(!!A&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!*!!!!#@````]!!!!"!!!!!A!!!!-!!!!%!!!!"1!!!!9!!!!(!!!!#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!'!#!!!!!!!!!!!!!!!!!!!%!!!!!!!A!!!!!#A"B!0%!!!!!!!!!!2.$&lt;WZO:7.U;7^O6(FQ:8-O9X2M!%6!&amp;A!'"EVP:'*V=QF.&lt;W2C&gt;8.51V!'5W6S;7&amp;M#%6U;'6S&lt;G6U!U2"51.65U)!!!Z$&lt;WZO:7.U;7^O6(FQ:1!!$E!B#5.P&lt;GZF9X2F:!!-1$$`````!EF1!!!21!I!#V*F&lt;7^U:3"1&lt;X*U!"*!-0````])1U^.)&amp;"P=H1!!!^!#A!)1G&amp;V:(*B&gt;'5!!!^!#A!)5X2P='*J&gt;(-!!!V!#A!'5'&amp;S;82Z!!!51(!!"1VD&lt;WZO:7.U;7^O)%F%!'1!]&gt;[0_:Y!!!!#%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=QZ$&lt;WZO:7.U;7^O,G.U&lt;!![1&amp;!!#1!!!!%!!A!$!!1!"1!'!!=!#"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!E!!!!*`````Q!!!!%!!!!#!!!!!Q!!!!1!!!!&amp;!!!!"A!!!!=!!!!)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!9!)!!!!!!!!!!!!!!!1!!!":$&lt;WZO:7.U;7^O37ZG&lt;SZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="Connection.ctl" Type="Class Private Data" URL="Connection.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Baudrate" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">Baudrate</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Baudrate</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="Read Baudrate.vi" Type="VI" URL="../Read Baudrate.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%G!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)1G&amp;V:(*B&gt;'5!!#Z!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$E.P&lt;GZF9X2J&lt;WYA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$5.P&lt;GZF9X2J&lt;WYA;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="Write Baudrate.vi" Type="VI" URL="../Write Baudrate.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%G!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$E.P&lt;GZF9X2J&lt;WYA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)1G&amp;V:(*B&gt;'5!!#R!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$5.P&lt;GZF9X2J&lt;WYA;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!*)!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="COM Port" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">COM Port</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">COM Port</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="Read COM Port.vi" Type="VI" URL="../Read COM Port.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!-0````])1U^.)&amp;"P=H1!!#Z!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$E.P&lt;GZF9X2J&lt;WYA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$5.P&lt;GZF9X2J&lt;WYA;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="Write COM Port.vi" Type="VI" URL="../Write COM Port.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$E.P&lt;GZF9X2J&lt;WYA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!-0````])1U^.)&amp;"P=H1!!#R!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$5.P&lt;GZF9X2J&lt;WYA;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!*)!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="Connected" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">Connected</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Connected</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="Read Connected.vi" Type="VI" URL="../Read Connected.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1F$&lt;WZO:7.U:71!,E"Q!"Y!!"131W^O&lt;G6D&gt;'FP&lt;CZM&gt;G.M98.T!!!/1W^O&lt;G6D&gt;'FP&lt;C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,%"Q!"Y!!"131W^O&lt;G6D&gt;'FP&lt;CZM&gt;G.M98.T!!!.1W^O&lt;G6D&gt;'FP&lt;C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="Write Connected.vi" Type="VI" URL="../Write Connected.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$E.P&lt;GZF9X2J&lt;WYA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!Z!)1F$&lt;WZO:7.U:71!,%"Q!"Y!!"131W^O&lt;G6D&gt;'FP&lt;CZM&gt;G.M98.T!!!.1W^O&lt;G6D&gt;'FP&lt;C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!EA!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="ConnectionType" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">ConnectionType</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ConnectionType</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="Read ConnectionType.vi" Type="VI" URL="../Read ConnectionType.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Y!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!'%!]1!!!!!!!!!"%U.P&lt;GZF9X2J&lt;WZ5?8"F=SZD&gt;'Q!25!7!!9'47^E9H6T#5VP:'*V=V2$5!:4:8*J97Q)282I:8*O:81$2%&amp;2!V641A!!$E.P&lt;GZF9X2J&lt;WZ5?8"F!!!O1(!!(A!!&amp;"*$&lt;WZO:7.U;7^O,GRW9WRB=X-!!!Z$&lt;WZO:7.U;7^O)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!M1(!!(A!!&amp;"*$&lt;WZO:7.U;7^O,GRW9WRB=X-!!!V$&lt;WZO:7.U;7^O)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
		</Item>
		<Item Name="Write ConnectionType.vi" Type="VI" URL="../Write ConnectionType.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Y!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$E.P&lt;GZF9X2J&lt;WYA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!'%!]1!!!!!!!!!"%U.P&lt;GZF9X2J&lt;WZ5?8"F=SZD&gt;'Q!25!7!!9'47^E9H6T#5VP:'*V=V2$5!:4:8*J97Q)282I:8*O:81$2%&amp;2!V641A!!$E.P&lt;GZF9X2J&lt;WZ5?8"F!!!M1(!!(A!!&amp;"*$&lt;WZO:7.U;7^O,GRW9WRB=X-!!!V$&lt;WZO:7.U;7^O)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574592</Property>
		</Item>
	</Item>
	<Item Name="IP" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">IP</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">IP</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="Read IP.vi" Type="VI" URL="../Read IP.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%D!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!-0````]#36!!!#Z!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$E.P&lt;GZF9X2J&lt;WYA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$5.P&lt;GZF9X2J&lt;WYA;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="Write IP.vi" Type="VI" URL="../Write IP.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%D!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$E.P&lt;GZF9X2J&lt;WYA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!R!-0````]#36!!!#R!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$5.P&lt;GZF9X2J&lt;WYA;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!*)!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="Parity" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">Parity</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Parity</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="Read Parity.vi" Type="VI" URL="../Read Parity.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!#A!'5'&amp;S;82Z!!!O1(!!(A!!&amp;"*$&lt;WZO:7.U;7^O,GRW9WRB=X-!!!Z$&lt;WZO:7.U;7^O)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!M1(!!(A!!&amp;"*$&lt;WZO:7.U;7^O,GRW9WRB=X-!!!V$&lt;WZO:7.U;7^O)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="Write Parity.vi" Type="VI" URL="../Write Parity.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$E.P&lt;GZF9X2J&lt;WYA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!V!#A!'5'&amp;S;82Z!!!M1(!!(A!!&amp;"*$&lt;WZO:7.U;7^O,GRW9WRB=X-!!!V$&lt;WZO:7.U;7^O)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="Remote Port" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">Remote Port</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Remote Port</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="Read Remote Port.vi" Type="VI" URL="../Read Remote Port.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%I!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!#A!,5G6N&lt;X2F)&amp;"P=H1!,E"Q!"Y!!"131W^O&lt;G6D&gt;'FP&lt;CZM&gt;G.M98.T!!!/1W^O&lt;G6D&gt;'FP&lt;C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,%"Q!"Y!!"131W^O&lt;G6D&gt;'FP&lt;CZM&gt;G.M98.T!!!.1W^O&lt;G6D&gt;'FP&lt;C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="Write Remote Port.vi" Type="VI" URL="../Write Remote Port.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%I!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$E.P&lt;GZF9X2J&lt;WYA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"&amp;!#A!,5G6N&lt;X2F)&amp;"P=H1!,%"Q!"Y!!"131W^O&lt;G6D&gt;'FP&lt;CZM&gt;G.M98.T!!!.1W^O&lt;G6D&gt;'FP&lt;C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!EA!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="Stopbits" Type="Property Definition">
		<Property Name="NI.ClassItem.Property.LongName" Type="Str">Stopbits</Property>
		<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Stopbits</Property>
		<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
		<Item Name="Read Stopbits.vi" Type="VI" URL="../Read Stopbits.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%G!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)5X2P='*J&gt;(-!!#Z!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$E.P&lt;GZF9X2J&lt;WYA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$5.P&lt;GZF9X2J&lt;WYA;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="Write Stopbits.vi" Type="VI" URL="../Write Stopbits.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%G!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$E.P&lt;GZF9X2J&lt;WYA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)5X2P='*J&gt;(-!!#R!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$5.P&lt;GZF9X2J&lt;WYA;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!*)!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="closeConnection.vi" Type="VI" URL="../closeConnection.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#3!!!!!Q!%!!!!-E"Q!"Y!!"131W^O&lt;G6D&gt;'FP&lt;CZM&gt;G.M98.T!!!31W^O&lt;G6D&gt;'FP&lt;CZM&gt;G.M98.T!!"5!0!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1-!!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!"!!)!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
	</Item>
	<Item Name="createConnection.vi" Type="VI" URL="../createConnection.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%*!!!!#1!A1$$`````&amp;E.P&lt;GZF9X2J&lt;WYA6(FQ:3"4&gt;(*J&lt;G=!!!1!!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!#!!-!"".F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$*!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!%E.P&lt;GZF9X2J&lt;WZ*&lt;G:P)%^66!!!&amp;E"1!!-!!A!$!!1*:8*S&lt;X)A&lt;X6U!%Y!]!!,!!!!!1!"!!5!!1!"!!%!"A!"!!%!"Q-!!/I!!1I!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!*!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">262400</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
	<Item Name="OpenConnection.vi" Type="VI" URL="../OpenConnection.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$E.P&lt;GZF9X2J&lt;WYA&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!$5.P&lt;GZF9X2J&lt;WYA;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="readConnectionInfo.vi" Type="VI" URL="../readConnectionInfo.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!';!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!5%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!%E.P&lt;GZF9X2J&lt;WYO&lt;(:D&lt;'&amp;T=Q!!'U!7!!%,9W^O:GFH)'2B&gt;'%!"H*F:GZV&lt;1!!31$RO;;7B!!!!!)24EF@4&amp;:$&lt;WZG;7=O&lt;(:M;7)71W^O:GFH)%2B&gt;'%A5G6G4H6N,G.U&lt;!!91(!!!1!"!!9)=G6G&lt;H6N)$)!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!2Q$RO;;7B!!!!!)24EF@4&amp;:$&lt;WZG;7=O&lt;(:M;7)71W^O:GFH)%2B&gt;'%A5G6G4H6N,G.U&lt;!!71(!!!1!"!!9'=G6G&lt;H6N!!"5!0!!$!!$!!1!"1!(!!1!"!!%!!1!#!!%!!1!#1-!!(A!!!E!!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!#A!!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
	</Item>
</LVClass>
